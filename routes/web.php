<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// define global variable
view()->share('version', time());
view()->share('host_name', 'ទូរទស្សន៍ជាតិកម្ពុជា ទទក - TVK');
view()->share('meta_description', 'National Television of Cambodia (TVK) was established in 1966 and it has been the only state television in Cambodia since the country gained independence on 9 November 1953. TVK broadcasts a variety of programs, educates that covers the whole country and reaches overseas viewers in Asia, Oceania, Europe, and the USA through satellite. The signal of TVK is received by 90 percent of Cambodia’s population.');
view()->share('img_root', '/');
view()->share('default_social_image', asset('img/logo-512.jpg'));

// ajax
Route::get('/ajax/vdo', 'AjaxController@vdo')->name('ajax-vdo');
Route::get('/ajax/program', 'AjaxController@program')->name('ajax-program');
Route::get('/ajax/search', 'AjaxController@search')->name('ajax-search');

// tag,search
Route::get('/tag/{slug}', 'TagController@index')->name('tag');

// page
Route::get('/program', 'ProgramController@index')->name('program');
Route::get('/program/{slug}', 'ProgramController@show')->name('program-show');

Route::get('/topic/{slug}', 'TopicController@index')->name('topic');
Route::get('/topic/{topic}/{id}', 'TopicController@show')->name('show-video');

Route::get('/category/{slug}', 'CategoryController@index')->name('category');
Route::get('/category/{category}/{id}', 'Api\ArticleController@show')->name('show-article');

/** CATCH-ALL ROUTE for Backpack/PageManager - needs to be at the end of your routes.php file  **/
Route::get('/{page}/{subs?}', ['uses' => '\App\Http\Controllers\PageController@index'])
    ->where(['page' => '^(((?=(?!admin))(?=(?!\/)).))*$', 'subs' => '.*']);