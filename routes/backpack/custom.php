<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

//Route::get('api/article', 'App\Http\Controllers\Api\ArticleController@index');
//Route::get('api/article-search', 'App\Http\Controllers\Api\ArticleController@search');

//Author or Manage Articles and Tags
Route::group([
    'namespace'  => 'Backpack\NewsCRUD\app\Http\Controllers\Admin',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'role:super admin,admin,editor,author']
], function () {
    Route::crud('article', 'ArticleCrudController');
    Route::crud('tag', 'TagCrudController');

    if (app('env') == 'production') {
        // disable delete and bulk delete for all CRUDs
        $cruds = ['article', 'category', 'tag', 'monster', 'icon', 'product', 'page', 'menu-item', 'user', 'role', 'permission'];
        foreach ($cruds as $name) {
            Route::delete($name.'/{id}', function () {
                return false;
            });
            Route::post($name.'/bulk-delete', function () {
                return false;
            });
        }
    }
});

//Editor or Manage Categories, Programs, Episodes, Videos, Topics and Pages
Route::group([
    'namespace'  => '',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'role:super admin,admin,editor']
], function () {
    Route::crud('category', 'Backpack\NewsCRUD\app\Http\Controllers\Admin\CategoryCrudController');
    Route::crud('program', 'App\Http\Controllers\Admin\ProgramCrudController');
    Route::crud('episode', 'App\Http\Controllers\Admin\EpisodeCrudController');
    Route::crud('video', 'App\Http\Controllers\Admin\VideoCrudController');
    Route::crud('topic', 'App\Http\Controllers\Admin\TopicCrudController');
    Route::crud('page', 'Backpack\PageManager\app\Http\Controllers\Admin\PageCrudController');
});

//Admin or Manage Menu Items, Users, Logs, Backups, Settings
Route::group([
    'namespace'  => 'Backpack',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'role:super admin,admin']
], function () { // custom admin routes

    Route::crud('menu-item', 'MenuCRUD\app\Http\Controllers\Admin\MenuItemCrudController');
    Route::crud('user', 'PermissionManager\app\Http\Controllers\UserCrudController');
    //Route::crud('setting', 'Settings\app\Http\Controllers\SettingCrudController');

    //Log
    Route::get('log', 'LogManager\app\Http\Controllers\LogController@index')->name('log.index');
    Route::get('log/preview/{file_name}', 'LogManager\app\Http\Controllers\LogController@preview')->name('log.show');
    Route::get('log/download/{file_name}', 'LogManager\app\Http\Controllers\LogController@download')->name('log.download');
    Route::delete('log/delete/{file_name}', 'LogManager\app\Http\Controllers\LogController@delete')->name('log.destroy');

    //Backup
    Route::get('backup', 'BackupManager\app\Http\Controllers\BackupController@index')->name('backup.index');
    Route::put('backup/create', 'BackupManager\app\Http\Controllers\BackupController@create')->name('backup.store');
    Route::get('backup/download/{file_name?}', 'BackupManager\app\Http\Controllers\BackupController@download')->name('backup.download');
    Route::delete('backup/delete/{file_name?}', 'BackupManager\app\Http\Controllers\BackupController@delete')->where('file_name', '(.*)')->name('backup.destroy');
});

//Admin or Manage Roles and Permissions
Route::group([
    'namespace'  => 'Backpack\PermissionManager\app\Http\Controllers',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'role:super admin']
], function () { // custom admin routes

    Route::crud('role', 'RoleCrudController');
    Route::crud('permission', 'PermissionCrudController');

});

//Manage Article
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage articles']],
    function() {
        Route::crud('article', 'Backpack\NewsCRUD\app\Http\Controllers\Admin\ArticleCrudController');
    }
);

//Manage Tag
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage tags']],
    function() {
        Route::crud('tag', 'Backpack\NewsCRUD\app\Http\Controllers\Admin\TagCrudController');
    }
);

//Manage Category
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage categories']],
    function() {
        Route::crud('category', 'Backpack\NewsCRUD\app\Http\Controllers\Admin\CategoryCrudController');
    }
);

//Manage Program
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage programs']],
    function() {
        Route::crud('program', 'App\Http\Controllers\Admin\ProgramCrudController');
    }
);

//Manage Episode
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage episodes']],
    function() {
        Route::crud('episode', 'App\Http\Controllers\Admin\EpisodeCrudController');
    }
);

//Manage Video
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage videos']],
    function() {
        Route::crud('video', 'App\Http\Controllers\Admin\VideoCrudController');
    }
);

//Manage Topic
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage topics']],
    function() {
        Route::crud('topic', 'App\Http\Controllers\Admin\TopicCrudController');
    }
);

//Manage Page
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage pages']],
    function() {
        Route::crud('page', 'Backpack\PageManager\app\Http\Controllers\Admin\PageCrudController');
    }
);

//Manage Page
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage menu items']],
    function() {
        Route::crud('menu-item', 'Backpack\MenuCRUD\app\Http\Controllers\Admin\MenuItemCrudController');
    }
);

//Manage User
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage users']],
    function() {
        Route::crud('user', 'Backpack\PermissionManager\app\Http\Controllers\UserCrudController');
    }
);

//Log
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:logs']],
    function() {
        Route::get('log', 'Backpack\LogManager\app\Http\Controllers\LogController@index')->name('log.index');
        Route::get('log/preview/{file_name}', 'Backpack\LogManager\app\Http\Controllers\LogController@preview')->name('log.show');
        Route::get('log/download/{file_name}', 'Backpack\LogManager\app\Http\Controllers\LogController@download')->name('log.download');
        Route::delete('log/delete/{file_name}', 'Backpack\LogManager\app\Http\Controllers\LogController@delete')->name('log.destroy');
    }
);

//Backup
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:backups']],
    function() {
        Route::get('backup', 'Backpack\BackupManager\app\Http\Controllers\BackupController@index')->name('backup.index');
        Route::put('backup/create', 'Backpack\BackupManager\app\Http\Controllers\BackupController@create')->name('backup.store');
        Route::get('backup/download/{file_name?}', 'Backpack\BackupManager\app\Http\Controllers\BackupController@download')->name('backup.download');
        Route::delete('backup/delete/{file_name?}', 'Backpack\BackupManager\app\Http\Controllers\BackupController@delete')->where('file_name', '(.*)')->name('backup.destroy');
    }
);

//Manage Role
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage roles']],
    function() {
        Route::crud('role', 'Backpack\PermissionManager\app\Http\Controllers\RoleCrudController');
    }
);

//Manage Permission
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware(),'can:manage permissions']],
    function() {
        Route::crud('permission', 'Backpack\PermissionManager\app\Http\Controllers\PermissionCrudController');
    }
);