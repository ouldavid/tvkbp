@extends('layouts.app')

@section('title', Str::replaceFirst('-', ' ', ucfirst($page->slug)).' | '.$host_name)
@section('social-title', $host_name)
@section('description', $meta_description)
@section('social-image', $default_social_image)

@section('content')
    <div class="bg-blue-dark">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="header-title line double-razor"><a href="{{$page->slug}}">{{$page->title}}</a></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gradient">
        <div class="container">
            <div class="bg-gray p-3 mx-n3 mx-sm-0">
                <div class="row">
                    <div class="col-12">
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
