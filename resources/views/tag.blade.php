@extends('layouts.app')

@section('title', $key_tag.' | '.$host_name)
@section('social-title', $host_name)
@section('description', $meta_description)
@section('social-image', $default_social_image)

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="bg-gradient">
        <div class="bg-blue-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="header-title line double-razor text-capitalize">{{ $articles->count() > 0 ? $key_tag : "Not Found" }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <!-- content -->
                <div class="col-md-12">
                    <div class="bg-gray p-3 mx-n3 mx-sm-0">
                        <div class="row">
                            @if($articles->count() > 0)
                                @foreach($articles as $key=>$article)
                                    <div class="col-lg-4 mb-3">
                                        <div class="card">
                                            <div class="row no-gutters">
                                                <div class="col-5 col-lg-12">
                                                    <a href="/category/{{ $article->category->slug }}/{{ $article->id }}" class="img-wrap-lazy">
                                                        <img class="card-img-top b-lazy" data-src="{{ $img_root.$article->image }}" alt="{{ $article->title }}">
                                                    </a>
                                                </div>
                                                <div class="col-7 col-lg-12">
                                                    <div class="card-body p-md-3 p-2">
                                                        <a href="/category/{{ $article->category->slug }}/{{ $article->id }}">
                                                            <h5 class="card-title mb-0">{{ $article->title }}</h5>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="col-12 text-center">
                                    <div class="error_number">
                                        <small>ERROR</small><br>
                                        404
                                        <hr>
                                    </div>
                                    <div class="error_title text-muted mb-3">
                                        Page not found.
                                    </div>
                                    <div class="error_description text-muted mb-5">
                                        <small>
                                            Please go back to our <a href="/">homepage</a>.
                                        </small>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
    {{--<script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
