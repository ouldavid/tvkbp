<nav id="navbar" class="navbar navbar-expand-lg navbar-dark bg-dark sidebarNavigation" data-sidebarClass="navbar-dark bg-dark">
    <div class="container-xl container-fluid">
        <a href="/" class="navbar-brand py-0">
            <img src="{{ asset('/img/logo.png') }}" alt="{{ $host_name }} Logo" class="brand-image elevation-3 img-fluid">
        </a>
        <button class="navbar-toggler leftNavbarToggler" type="button" data-toggle="" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <button type="button" class="close d-lg-none" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <a href="/" class="second-brand d-flex justify-content-center d-lg-none mb-5">
                <img src="{{ asset('/img/logo.png') }}" alt="{{ $host_name }} Logo" class="brand-image elevation-3">
            </a>
            <ul class="nav navbar-nav nav-flex-icons m-auto">
                @foreach($menu_items as $key=>$menu_item)
                    @if($key==0)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ $menu_item->link }}">
                                <i class="{{ config('global.icon_home') }}"></i>
                                <span>{{ $menu_item->name }}</span>
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                    @elseif($key==1)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ $menu_item->link }}">
                                <i class="{{ config('global.icon_globe') }}"></i>
                                <span>{{ $menu_item->name }}</span>
                            </a>
                        </li>
                    @elseif($key>= count($menu_items)-2)
                        <li class="nav-item d-block d-lg-none">
                            <a class="nav-link" href="/{{ $menu_item->page->slug }}">
                                <span>{{ $menu_item->name }}</span>
                            </a>
                        </li>
                    @elseif($menu_item->type == 'internal_link' or $menu_item->type =='page')
                        <li class="nav-item">
                            <a class="nav-link" href="/{{ $menu_item->link }}">
                                <span>{{ $menu_item->name }}</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="/{{ str_replace("_link","",$menu_item->type) }}/{{ $menu_item->link }}">
                                <span>{{ $menu_item->name }}</span>
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul><!-- .navbar-nav -->
            <!-- social mobile -->
            <ul class="social d-lg-none">
                <li class="nav-item mx-2">
                    <a href="https://www.facebook.com/tvkchannel7" target="_blank" class="icon-button facebook"><i class="fab fa-facebook-f"></i><span></span></a>
                </li>
                <li class="nav-item mx-2">
                    <a href="https://www.youtube.com/channel/UCupDvu26QCG36ORfsCXRq6A" target="_blank" class="icon-button bg-youtube youtube"><i class="fab fa-youtube"></i><span></span></a>
                </li>
                <li class="nav-item mx-2">
                    <a href="#" target="_blank" class="icon-button instagram"><i class="fab fa-instagram"></i><span></span></a>
                </li>
            </ul>
        </div><!-- #navbarsExampleDefault -->

        <div class="nav-right d-flex">
            <ul class="social d-inline-block mb-0">
                <li class="nav-item">
                    <a href="https://www.facebook.com/tvkchannel7" target="_blank" class="icon-button facebook"><i class="fab fa-facebook-f"></i><span></span></a>
                </li>
                <li class="nav-item">
                    <a href="#" target="_blank" class="icon-button bg-instagram instagram"><i class="fab fa-instagram"></i><span></span></a>
                </li>
                <li class="nav-item">
                    <a href="https://www.youtube.com/channel/UCupDvu26QCG36ORfsCXRq6A" target="_blank" class="icon-button bg-youtube youtube"><i class="fab fa-youtube"></i><span></span></a>
                </li>
            </ul>
            <ul class="d-inline-block mb-0">
                <li class="nav-item pr-0">
                    <button id="search-btn"><i class="fas fa-search"></i></button>
                </li>
            </ul>
        </div>

    </div>
</nav>

<!-- search overlay -->
<div class="search-overlay">
    <button id="close-btn" type="button" class="close" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <form action="">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 col-sm-12 offset-sm-0">
                    <div class="input-group form-btn">
                        <input type="text" name="txt_search" class="form-control border-bottom search-input" placeholder="ស្វែងរក..." aria-describedby="button-submit">
                        <div class="input-group-append">
                            <button class="btn btn-outline-light brd-left-none border-bottom" type="button" id="button-submit"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                    <div class="search-result position-relative p-3">
                        <!-- search -->
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>

