<div id="container-sidebar">
    <div class="mx-3 py-3">
        <h2 class="header-title text-center color-black py-3 mb-3">{{ AppHelper::sidebar_article()[1]->name }}</h2>

        @foreach(AppHelper::sidebar_article()[0]->slice(0, 5) as $article)
            <div class="card mb-2">
                <div class="row no-gutters">
                    <div class="col-lg-5 col-md-12 col-5">
                        <a href="/category/{{ $article->category->slug }}/{{ trim($article->id) }}" class="img-wrap-lazy">
                            <img class="card-img b-lazy" data-src="{{ $img_root.trim($article->image) }}" alt="{{ trim($article->title) }}">
                        </a>
                    </div>
                    <div class="col-lg-7 col-md-12 col-7">
                        <div class="card-body p-2">
                            <a href="/category/{{ $article->category->slug }}/{{ $article->id }}">
                                <h5 class="card-title font-weight-normal mb-0">{{ trim($article->title) }}</h5>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        <h2 class="header-title text-center color-black py-3 mb-3">កម្មវិធីទទក</h2>
        <div class="row">
            <div class="col-12">
                <div id="sidebar-program">
                    @foreach(AppHelper::sidebar_program()->slice(0, 3) as $program)
                    <div class="slide mb-2">
                        <a href="/program/{{ trim($program->slug) }}"><img class="img-fluid" src="{{ $img_root.trim($program->image) }}" alt="{{ trim($program->title) }}" /></a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
