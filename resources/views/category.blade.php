@extends('layouts.app')

@section('title', $category->slug.' | '.$host_name)
@section('social-title', $host_name)
@section('description', $meta_description)
@section('social-image', $default_social_image)

@section('css')
    <!-- page css -->
    {{--<link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">--}}
@endsection

@section('content')
    <div class="bg-gradient">
        <div class="bg-blue-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="header-title line double-razor">{{ $category->name }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <!-- content -->
                <div class="col-md-12">
                    <div class="bg-block py-3 px-md-3 px-0">
                        <div class="row">
                            @foreach($articles as $key=>$article)
                                @if($key < 2)
                                    <div class="col-12 col-md-6 mb-3">
                                        <div class="card">
                                            <a href="/category/{{ $category->slug }}/{{ $article->id }}" class="img-wrap-lazy">
                                                <img class="card-img-top b-lazy" data-src="{{ $img_root.$article->image }}" alt="{{ $article->title }}">
                                            </a>
                                            <div class="card-body p-3">
                                                <a href="/category/{{ $category->slug }}/{{ $article->id }}">
                                                    <h5 class="card-title mb-0">{{ $article->title }}</h5>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="col-lg-4 mb-3">
                                        <div class="card">
                                            <div class="row no-gutters">
                                                <div class="col-5 col-lg-12">
                                                    <a href="/category/{{ $category->slug }}/{{ $article->id }}" class="img-wrap-lazy">
                                                        <img class="card-img-top b-lazy" data-src="{{ $img_root.$article->image }}" alt="{{ $article->title }}">
                                                    </a>
                                                </div>
                                                <div class="col-7 col-lg-12">
                                                    <div class="card-body p-md-3 p-2">
                                                        <a href="/category/{{ $category->slug }}/{{ $article->id }}">
                                                            <h5 class="card-title mb-0">{{ $article->title }}</h5>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
    {{--<script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
