@if($data_search->count() > 0)
    @foreach($data_search->take(10) as $item)
        <div class="show border-bottom mb-3" align="left" onClick="return true">
            <a href="/category/{{ $item->category->slug }}/{{ $item->id }}">
                <span id="{{ $item->id }}" class="name">@php echo str_ireplace($key_search, "<b>$key_search</b>", $item->title); @endphp</span>
            </a>
        </div>
    @endforeach
    <script type="text/javascript">
        $(".search-result").fadeIn();
        $(".search-input").click(function() {
            $(".search-result").css("display","block");
        });
    </script>
@else
   <script type="text/javascript">
       $(".search-result").css("display","none");
       $(".search-input").click(function() {
           $(".search-result").css("display","none");
       });
   </script>
@endif
<div class="ajax-loader"><img src="{{ asset('img/ajax-loader.gif') }}"/></div>
<script>
    $(document).ajaxStart(function(){
        $(".ajax-loader").fadeIn(300);
    }).ajaxStop(function(){
        $(".ajax-loader").fadeOut(300);
    });
</script>
