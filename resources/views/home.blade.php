@extends('layouts.app')

@section('title', $host_name)
@section('social-title', $host_name)
@section('description', $meta_description)
@section('social-image', $default_social_image)

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower($page->template).'.css?v='.$version) }}">
@endsection

@section('banner')
    <div class="bg-blue-dark">
        <div class="container py-3">
            <div class="row">
                <div class="col-xl-8 pr-xl-2 px-xs-0 mb-3 mb-xl-0">
                    <div id="banner-slide">
                        @foreach($articles->where('featured',1)->take($page->extras['slider_count']) as $article)
                            <div class="slide d-flex justify-content-center">
                                <a href="/category/{{$article->category->slug}}/{{$article->id}}">
                                    <img class="img-fluid" src="{{ $img_root.$article->image }}" alt="{{ $article->title }}" />
                                </a>
                                <div class="carousel-caption p-0">
                                    <a href="/category/{{$article->category->slug}}" class="btn-sm tags bg-red mx-3">{{ $article->category->name }}</a>
                                    <div class="bg-caption py-2 px-3 py-md-3">
                                        <h3>{{ $article->title }}</h3>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-xl-4 pl-xl-2">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-center mb-3">
                            <video id="live1" data-source="{{ $page->extras['tvk_url'] }}" width="100%" height="100%" controls autoplay muted></video>
                        </div>
                        <div class="col-md-12 d-flex justify-content-center">
                            <video id="live2" data-source="{{ $page->extras['tvk2_url'] }}" width="100%" height="100%" controls autoplay muted></video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="bg-gradient">
        <div class="container py-3">
            @if($articles->count() > 0)
            <h2 class="header-title line double-razor"><a href="/category/{{ $page->extras['row_1'] }}">{{ $articles->where('category.slug',$page->extras['row_1'])->first()->category->name }}</a></h2>
            <div class="row">
                @foreach($articles->where('category.slug',$page->extras['row_1'])->take(3) as $article)
                    <div class="col-md-4 mb-3">
                        <div class="card">
                            <a href="/category/{{$article->category->slug}}/{{$article->id}}" class="img-wrap-lazy">
                                <img class="card-img-top b-lazy" data-src="{{ $img_root.$article->image }}" alt="{{ $article->title }}">
                            </a>
                            <div class="card-body p-3">
                                <a href="/category/{{$article->category->slug}}/{{$article->id}}">
                                    <h5 class="card-title mb-0">{{ $article->title }}</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div><!-- tvk new -->
            @endif

            @if($programs->count() > 0)
            <h2 class="header-title line double-razor"><a href="/{{ $page->extras['row_2'] }}">កម្មវិធីទទក</a></h2>
            <div class="row">
                <div class="col-12">
                    <div class="program mb-3">
                        @foreach($programs as $program)
                            <div class="slide">
                                <a href="/program/{{ $program->slug }}"><img class="img-fluid" src="{{ $img_root.$program->image }}" alt="{{ $program->name }}" /></a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div><!-- .program -->
            @endif

            @if($videos->count() > 0)
            <div class="row">
                @for($i=1;$i<=3;$i++)
                    <div class="col-md-4">
                        <h2 class="header-title line double-razor"><a href="/topic/{{ $page->extras['column_'.$i] }}">{{ $videos->where('topic.slug',$page->extras['column_'.$i])->first()->topic->name }}</a></h2>
                        @foreach($videos->where('topic.slug',$page->extras['column_'.$i])->take(3) as $video)
                            <div class="card mb-3">
                                <div class="img-wrap-lazy position-relative">
                                    <a href="#" class="d-block video-btn" data-toggle="modal" data-id='{{ $video->id }}' data-table='videos' data-target="#modal-vdo">
                                        <img class="card-img-top b-lazy" data-src="{{ $img_root.$video->image }}" alt="{{ $video->title }}">
                                    </a>
                                    <a href="/topic/{{ $video->topic->slug }}" class="btn-sm tags position-absolute bottom left bg-red">{{ $video->topic->name }}</a>
                                </div>
                                <div class="card-body p-3">
                                    <a href="#" class="video-btn" data-toggle="modal" data-id='{{ $video->id }}' data-table='videos' data-target="#modal-vdo">
                                        <h5 class="card-title mb-0">{{ $video->title }}</h5>
                                    </a>
                                </div>
                                <a href="" class="btn-sm btn-play top mx-3 my-2 video-btn" data-toggle="modal" data-id='{{ $video->id }}' data-table='videos' data-target="#modal-vdo"><i class="fas fa-play"></i></a>
                            </div>
                        @endforeach
                    </div>
                @endfor
            </div>
            @endif
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade modal-vdo" id="modal-vdo" tabindex="-1" role="dialog" aria-labelledby="vdoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="video-embed">
                        <!-- element append -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- stream player -->
    <script type="text/javascript" src="{{ asset('plugins/hls/hls.min.js') }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower($page->template).'.js?v='.$version) }}"></script>
@endsection
