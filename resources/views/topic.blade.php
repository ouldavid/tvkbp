@extends('layouts.app')

@section('title', $topic->name.' | '.$host_name)
@section('social-title', $host_name)
@section('description', $meta_description)
@section('social-image', $default_social_image)

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="bg-gradient">
        <div class="bg-blue-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="header-title line double-razor">{{ $topic->name }}</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <!-- content -->
                <div class="col-md-8">
                    <div class="bg-block py-3 px-md-3 px-0">
                        <div class="row">
                            @foreach($videos as $key=>$video)
                                @if($key < 1)
                                    <div class="col-12 mb-3">
                                        <div class="card">
                                            <div class="img-wrap-lazy position-relative">
                                                <a href="#" class="d-block video-btn" data-toggle="modal" data-id='{{ $video->id }}' data-table='videos' data-target="#modal-vdo">
                                                    <img class="card-img-top b-lazy" data-src="{{ $img_root.trim($video->image) }}" alt="{{ trim($video->title) }}">
                                                </a>
                                                <div class="carousel-caption p-0">
                                                    <a href="/topic/{{ trim($video->topic->slug) }}" class="btn-sm tags bg-red mx-3">{{ trim($video->topic->name) }}</a>
                                                    <div class="bg-caption py-2 px-3 py-md-3">
                                                        <h3>{{ trim($video->title) }}</h3>
                                                    </div>
                                                </div>
                                                <a href="#" class="btn-sm btn-play top right mx-3 my-2 video-btn" data-toggle="modal" data-id='{{ $video->id }}' data-table='videos' data-target="#modal-vdo"><i class="fas fa-play"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="col-12 col-md-6 mb-3">
                                        <div class="card">
                                            <div class="img-wrap-lazy position-relative">
                                                <a href="#" class="d-block video-btn" data-toggle="modal" data-id='{{ $video->id }}' data-table='videos' data-target="#modal-vdo">
                                                    <img class="card-img-top b-lazy" data-src="{{ $img_root.trim($video->image) }}" alt="{{ trim($video->title) }}">
                                                </a>
                                                <div class="carousel-caption p-0">
                                                    <a href="/topic/{{ trim($video->topic->slug) }}" class="btn-sm tags bg-red mx-3">{{ trim($video->topic->name) }}</a>
                                                    <div class="bg-caption py-2 px-3 py-md-3">
                                                        <h5>{{ trim($video->title) }}</h5>
                                                    </div>
                                                </div>
                                                <a href="#" class="btn-sm btn-play top right mx-3 my-2 video-btn" data-toggle="modal" data-id='{{ $video->id }}' data-table='videos' data-target="#modal-vdo"><i class="fas fa-play"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- sidebar -->
                <div class="col-md-4">
                    <!-- include sidebar -->
                    @include('templates.sidebar')
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade modal-vdo" id="modal-vdo" tabindex="-1" role="dialog" aria-labelledby="vdoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="video-embed">
                        <!-- element append -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- sidebar js -->
    <script type="text/javascript" src="{{ asset('js/sidebar.js?v='.$version) }}"></script>
    <!-- page js -->
    {{--<script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
