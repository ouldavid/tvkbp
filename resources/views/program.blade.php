@extends('layouts.app')

@section('title', Str::replaceFirst('-', ' ', ucfirst(Route::currentRouteName())).' | '.$host_name)
@section('social-title', $host_name)
@section('description', $meta_description)
@section('social-image', $default_social_image)

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="bg-blue-dark">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="header-title line double-razor">កម្មវិធីទទក</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gradient">
        <div class="container">
            <div class="bg-block py-3 px-md-3 px-0">
                <div class="row">
                    @foreach($programs as $program)
                    <div class="col-md-4 mb-3">
                        <a href="program/{{ trim($program->slug) }}" class="img-wrap-lazy lazy-square">
                            <img class="img-fluid b-lazy" data-src="{{ $img_root.trim($program->image) }}" alt="{{ trim($program->title) }}" />
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
    {{--<script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
