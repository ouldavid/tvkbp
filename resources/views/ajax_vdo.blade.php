{!! trim($embed) !!}
<div class="ajax-loader"><img src="{{ asset('img/ajax-loader.gif') }}"/></div>

<script>
    $(document).ajaxStart(function(){
        $(".ajax-loader").fadeIn(300);
    }).ajaxStop(function(){
        $(".ajax-loader").fadeOut(300);
    });
</script>