@extends('layouts.app')

@section('title', $article->title.' | '.$host_name)
@section('social-title', $article->title)
@section('description', Str::limit(strip_tags($article->content),250,'...'))
@section('social-image', $article->image)

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/press.css?v='.$version) }}">
@endsection

@section('content')
    <div class="bg-gradient">
        <div class="bg-blue-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="header-title line double-razor"><a href="/category/{{$article->category->slug}}">{{ $article->category->name }}</a></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <!-- content -->
                <div class="col-md-8">
                    <div class="bg-block py-3 px-3 mb-3">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="article-title">{{ $article->title }}</h1>
                                <div class="title-footer mt-3">
                                    <i class="far fa-clock"></i>
                                    <span class="pull-left date">{{ $article->created_at->format('d-M-Y') }}</span>
                                    <!-- ShareThis BEGIN -->
                                    <div class="sharethis-inline-share-buttons float-right"></div>
                                    <!-- ShareThis END -->
                                </div>
                                <hr>
                                <div id="post-content" class="mt-3">
                                    {!! $article->content !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- sidebar -->
                <div class="col-md-4">
                    <!-- include sidebar -->
                    @include('templates.sidebar')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- sidebar js -->
    <script type="text/javascript" src="{{ asset('js/sidebar.js?v='.$version) }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/press.js?v='.$version) }}"></script>
@endsection
