@extends('layouts.app')

@section('title', $program->title.' | '.$host_name)
@section('social-title', $host_name)
@section('description', Str::limit(strip_tags($program->content),250,'...'))
@section('social-image', $program->image)

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/program.css?v='.$version) }}">
@endsection

@section('content')
    <div class="bg-blue-dark">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="header-title line double-razor">{{ trim($program->title) }}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gradient">
        <div class="container">
            <div class="bg-gray p-3 mx-n3 mx-sm-0">
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="img-wrap-lazy lazy-square shadow-sm">
                            <img class="img-fluid b-lazy" data-src="{{ $img_root.trim($program->image) }}" alt="{{ trim($program->title) }}" />
                        </div>
                    </div>
                    <div class="col-md-8 mb-3">
                        {!! trim($program->content) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <hr>
                        <div class="eps">
                            <ul>
                                @foreach($program->episodes as $episode)
                                <li>
                                    <span class="btn-sm bg-blue-dark text-white border-0 rounded-0 mr-3">{{ $episode->created_at->format('d-M-Y') }}</span>
                                    <a href="#" class="video-btn" data-id="{{ $episode->id }}" data-table='episodes' data-toggle="modal" data-target="#modal-vdo">{{ $episode->name }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade modal-vdo" id="modal-vdo" tabindex="-1" role="dialog" aria-labelledby="vdoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="video-embed">
                        <!-- element append -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
    {{--<script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
