@extends('layouts.app')

@section('title', $video->title.' | '.$host_name)
@section('social-title', $video->title)
@section('description', Str::limit(strip_tags($video->title),250,'...'))
@section('social-image', $video->image)

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/press.css?v='.$version) }}">
@endsection

@section('content')
    <div class="bg-gradient">
        <div class="bg-blue-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="header-title line double-razor"><a href="/topic/{{$video->topic->slug}}">{{ $video->topic->name }}</a></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <!-- content -->
                <div class="col-md-8">
                    <div class="bg-block py-3 px-3 mb-3">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="article-title">{{ $video->title }}</h1>
                                <div class="title-footer">
                                    <i class="far fa-clock"></i>
                                    <span class="pull-left date">{{ $video->created_at->format('d-M-Y') }}</span>
                                </div>
                                <hr>
                                <div id="post-content" class="mt-3">
                                    {!! $video->url !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- sidebar -->
                <div class="col-md-4">
                    <!-- include sidebar -->
                    @include('templates.sidebar')
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- sidebar js -->
    <script type="text/javascript" src="{{ asset('js/sidebar.js?v='.$version) }}"></script>
    <!-- page js -->
    {{--<script type="text/javascript" src="{{ asset('js/press.js?v='.$version) }}"></script>--}}
@endsection
