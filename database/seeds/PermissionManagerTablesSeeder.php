<?php

use Backpack\PermissionManager\app\Models\Permission;
use Backpack\PermissionManager\app\Models\Role;
use Illuminate\Database\Seeder;

class PermissionManagerTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'super admin', 'guard_name' => 'web'])->users()->sync([1]);
        Role::create(['name' => 'admin', 'guard_name' => 'web']);
        Role::create(['name' => 'editor', 'guard_name' => 'web']);
        Role::create(['name' => 'author', 'guard_name' => 'web']);

        Permission::create(['name' => 'manage articles', 'guard_name' => 'web'])->roles()->sync([1, 2, 3]);
        Permission::create(['name' => 'manage categories', 'guard_name' => 'web'])->roles()->sync([1, 2, 3]);
        Permission::create(['name' => 'manage tags', 'guard_name' => 'web'])->roles()->sync([1, 2, 3]);
        Permission::create(['name' => 'manage videos', 'guard_name' => 'web'])->roles()->sync([1, 2, 3]);
        Permission::create(['name' => 'manage topics', 'guard_name' => 'web'])->roles()->sync([1, 2, 3]);
        Permission::create(['name' => 'manage programs', 'guard_name' => 'web'])->roles()->sync([1, 2, 3]);
        Permission::create(['name' => 'manage episodes', 'guard_name' => 'web'])->roles()->sync([1, 2, 3]);
        Permission::create(['name' => 'manage pages', 'guard_name' => 'web'])->roles()->sync([1, 2, 3]);
        Permission::create(['name' => 'manage menu items', 'guard_name' => 'web'])->roles()->sync([1, 2]);
        Permission::create(['name' => 'manage users', 'guard_name' => 'web'])->roles()->sync([1, 2]);
        Permission::create(['name' => 'manage roles', 'guard_name' => 'web'])->roles()->sync([1]);
        Permission::create(['name' => 'manage permissions', 'guard_name' => 'web'])->roles()->sync([1]);
        Permission::create(['name' => 'file manager', 'guard_name' => 'web'])->roles()->sync([1, 2]);
        Permission::create(['name' => 'logs', 'guard_name' => 'web'])->roles()->sync([1, 2]);
        Permission::create(['name' => 'backups', 'guard_name' => 'web'])->roles()->sync([1, 2]);
        Permission::create(['name' => 'settings', 'guard_name' => 'web'])->roles()->sync([1, 2]);
    }
}
