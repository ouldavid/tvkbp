<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users')->where('email', 'admin@tvk.gov.kh')->count() == 0) {
            DB::table('users')->insert([
                'name'     => 'Admin',
                'email'    => 'admin@tvk.gov.kh',
                'password' => bcrypt('TVK@2020!@#'),
            ]);
        }

        /*factory(User::class, 131)->create();*/
    }
}
