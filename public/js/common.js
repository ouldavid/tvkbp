jQuery(document).ready(function($) {
    // active menu
    var url = window.location;
    $('.navbar-nav a[href="'+ url +'"]').parent().addClass('active');
    $('.navbar-nav a').filter(function() {
        return this.href == url;
    }).parent().addClass('active');
    // active dropdown menu
    $('.navbar-nav .dropdown a[href="'+ url +'"]').parents('li.dropdown').addClass('active');
    $('.navbar-nav .dropdown a').filter(function() {
        return this.href == url;
    }).parents('li.dropdown').addClass('active');

    // search overlay
    $("button#search-btn").click(function() {
        $(".search-overlay").fadeIn();
        $("html, body").css("overflow", "hidden");
    });
    $(".search-overlay button#close-btn").click(function() {
        $("html, body").css("overflow", "auto");
        $(".search-overlay").fadeOut();
        $(".search-overlay").close();
    });

    // button close, advertising
    $("#ads-header button.close").click(function() {
        $(this).parent().hide();
    });

    // b-lazy image
    var bLazy = new Blazy({
        breakpoints: [
            {
                width: 575.98, // max-width
                src  : 'data-src-xs'
            }, {
                width: 767.98, // max-width
                src  : 'data-src-sm'
            }, {
                width: 991.98, // max-width
                src  : 'data-src-md'
            }
        ],
        success: function(element) {
            setTimeout(function() {
                // We want to remove the loader gif now.
                // First we find the parent container
                // then we remove the "loading" class which holds the loader image
                var parent = element.parentNode;
                parent.className = parent.className.replace(/\bloading\b/,'');
            }, 200);
        }
    });

    // Gets the video src from the data-src on each button
    $('.video-btn').click(function() {
        const vdoid = $(this).data("id");
        const table = $(this).data("table");
        if(vdoid) {
            $.ajax({
                type: "GET",
                url: "/ajax/vdo/",
                data: {id:vdoid,table:table},
                success:function(result) {
                    if(result) {
                        $(".modal .video-embed").html(result);
                    }
                }
            })
        }
    });
    // stop playing the youtube video when I close the modal
    $('#modal-vdo').on('hide.bs.modal', function (e) {
        // a poor man's stop video
        $(".modal .video-embed").empty();
    });

    // load function search
    data_search();

});

// header position fixed
window.onscroll = function() {navbarSticky()};
var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;
function navbarSticky() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
}

// Navigation Header
window.onload=function() {
    if(window.jQuery) {
        $(document).ready(function() {
            $(".sidebarNavigation .navbar-collapse").hide().clone().appendTo("body").removeAttr("class").addClass("sideMenu").show();
            $("body").append("<div class='overlay'></div>");
            $(".navbar-toggle, .navbar-toggler").on("click",function() {
                $(".sideMenu").addClass($(".sidebarNavigation").attr("data-sidebarClass"));
                $(".sideMenu, .overlay").toggleClass("open");
                $(".overlay").on("click",function(){$(this).removeClass("open");$(".sideMenu").removeClass("open")})
            });
            $("body").on("click",".sideMenu.open .close",function() {
                $(".sideMenu, .overlay").toggleClass("open");
            });
           /* $("body").on("click",".sideMenu.open .nav-item",function() {
                if(!$(this).hasClass("dropdown")){$(".sideMenu, .overlay").toggleClass("open")}
            });*/
            $(window).resize(function() {
                if($(".navbar-toggler").is(":hidden")){$(".sideMenu, .overlay").hide()}else{$(".sideMenu, .overlay").show()}
            })
        })
    }else{console.log("sidebarNavigation Requires jQuery")}
}

/**
 * SEARCH TITLE DROP BOX
 * ------------------------------------------------------------------------------
 */
// stop submit key enter 
function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}
document.onkeypress = stopRKey;
// search drop box
function data_search() {
    $(".search-input").keyup(function(event) {
        const search_val = $(this).val();

        if(search_val) {
            $.ajax({
                type	: "GET",
                url		: "/ajax/search",
                data    : {key_search:search_val},
                cache	: false,
                success	: function(result) {
                    $(".search-result").html(result);
                }
            });

            if(event.keyCode == 13) {
                // Enter key
                window.location.href="/tag/"+search_val.trim();
                /*key_search = search_val.split(' ').join('+');
                if(key_search != ""){
                    window.location.href="/tag/"+key_search;
                }*/
            }

            $(document).on('click', '.search-overlay #button-submit', function(e) {
                window.location.href="/tag/"+search_val.trim();
                /*key_search = search_val.split(' ').join('+');
                if(key_search != ""){
                    window.location.href="/tag/"+key_search;
                }*/
            });
        }else {
            $(".search-result").fadeOut();
            return false;
        }
    });

    $(document).on('click', '.show', function(e) {
        $name = $('span.name', this).html();
        var decoded = $("<div/>").html($name).text();
        $('.search-input').val(decoded);

        // click submit
        /* var search_val = $('span.name', this).attr("id");		
        if(search_val != ""){
            window.location.href="/content/"+search_val;
        } */
    });

    $(document).on('click', function(e) {
        var $clicked = $(e.target);
        if(! $clicked.hasClass("search-input")) {
            $(".search-result").fadeOut();
        }
    });
}

// reload table
function reload_page() {
	window.location.href=window.location.href;
}
