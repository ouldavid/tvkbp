$(document).ready(function() {
    // banner slide
    $('#banner-slide').slick({
        slidesToShow: 1,
        infinite: true,
        autoplay: true,
        dots: false,
        arrows: false,
        speed: 500,
        autoplaySpeed: 5000,
        //lazyLoad: 'ondemand',
        cssEase: 'linear'
    });

    // programs
    $('.program').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        autoplay: true,
        dots: false,
        arrows: true,
        nextArrow: '<button class="right d-flex align-items-center bg-transparent border-0"><img src="../img/arrow/arrow.png" alt="arrow right"></button>',
        prevArrow: '<button class="left d-flex align-items-center bg-transparent border-0"><img src="../img/arrow/arrow.png" alt="arrow left"></button>',
        speed: 500,
        autoplaySpeed: 5000,
        //lazyLoad: 'ondemand',
        cssEase: 'linear',
        responsive: [
            {
                breakpoint: 767.98,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 575.98,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    autoplay: true,
                }
            }
        ]
    });

    for (let i=1; i<=2; i++) {
        const video = document.getElementById('live' + i);
        if(Hls.isSupported()) {
            const hls = new Hls();
            hls.loadSource(video.dataset.source);
            hls.attachMedia(video);
            hls.on(Hls.Events.MANIFEST_PARSED,function() {
                video.play();
            });
        }
            // hls.js is not supported on platforms that do not have Media Source Extensions (MSE) enabled.
            // When the browser has built-in HLS support (check using `canPlayType`), we can provide an HLS manifest (i.e. .m3u8 URL) directly to the video element throught the `src` property.
        // This is using the built-in support of the plain video element, without using hls.js.
        else if (video.canPlayType('application/vnd.apple.mpegurl')) {
            video.src = video.dataset.source;
            video.addEventListener('canplay',function() {
                video.play();
            });
        }
    }

});
