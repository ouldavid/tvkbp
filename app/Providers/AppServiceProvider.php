<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Backpack\MenuCRUD\app\Models\MenuItem;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->share('menu_items', MenuItem::orderBy('lft')->get());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // register the services that are only used for development
    }
}
