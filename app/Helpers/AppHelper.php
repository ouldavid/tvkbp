<?php

namespace App\Helpers;

use App\Models\Program;
use Backpack\NewsCRUD\app\Models\Article;
use Backpack\NewsCRUD\app\Models\Category;

class AppHelper
{
    public static function sidebar_article() {
        $category = Category::findBySlug("press");
        $articles = Article::where('status','PUBLISHED')
            ->orwhere('category_id',$category->id)
            ->orderBy('created_at','DESC')
            ->get();
        return [$articles, $category];
    }

    public static function sidebar_program() {
        $programs = Program::where('status','PUBLISHED')
            ->orderBy('created_at','DESC')
            ->get();
        return $programs;
    }
}