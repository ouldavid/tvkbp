<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $permissions
     * @return mixed
     */
    public function handle($request, Closure $next, ...$permissions)
    {
        if (! $request->user()->hasAnyPermission($permissions)) {
            abort(403);
        }

        return $next($request);
    }
}
