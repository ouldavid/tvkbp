<?php

namespace App\Http\Controllers;

use Backpack\NewsCRUD\app\Models\Article;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @param $slug
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($slug)
    {
        $articles = Article::whereHas('tags', function($query) use ($slug) {
            $query->whereName($slug);
        })
        ->orWhere('title', 'LIKE', "%{$slug}%")
        ->where('status','PUBLISHED')
        ->orderBy('date','DESC')
        ->get();
        if($articles->count() > 0) {
            $key_tag = $slug;
        }else {
            $key_tag = "Error 404";
        }
        // view page
        return view('tag', compact('articles','key_tag'));
    }

}
