<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $programs = Program::where('status','PUBLISHED')
            ->orderBy('created_at','DESC')
            ->get();
        return view('program', compact('programs'));
    }
    public function show(Request $request)
    {
        $program = Program::where('slug',$request->slug)->first();

        return view('program_show',compact('program'));
    }
}
