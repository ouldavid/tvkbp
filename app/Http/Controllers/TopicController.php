<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use App\Models\Video;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    public function index($slug)
    {
        $topic = Topic::findBySlug($slug);

        if (!$topic)
        {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $videos = Video::where(['topic_id'=>$topic->id,'status'=>'PUBLISHED'])
            ->orderBy('created_at','DESC')
            ->get();
        return view('topic', compact(['topic','videos']));
    }

    public function show(Request $request)
    {
        $video = Video::find($request->id);
        return view('video',compact('video'));
    }
}
