<?php

namespace App\Http\Controllers;

use App\Models\Episode;
use App\Models\Video;
use Backpack\NewsCRUD\app\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    function vdo(Request $request) {
        if($request->ajax()) {
            $url = DB::table($request->table)->find($request->id)->url;
            $embed = $this->checkEmbed($url);
            // view ajax
            return view('ajax_vdo', compact('embed'));
        }
    }

    function checkEmbed($url){
        if (strpos($url, 'youtube') > 0 or strpos($url, 'youtu.be') > 0) {
            //get youtube video id
            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
            $embed='
                <div data-oembed-url="'.$url.'">
                    <div style="left: 0; width: 100%; height: 0; position: relative; padding-bottom: 56.25%;">
                        <iframe allow="encrypted-media; accelerometer; gyroscope; picture-in-picture" allowfullscreen="" scrolling="no" src="https://www.youtube.com/embed/'.$match[1].'?rel=0" style="border: 0; top: 0; left: 0; width: 100%; height: 100%; position: absolute;" tabindex="-1"></iframe>
                    </div>
                </div>';
        } elseif (strpos($url, 'facebook') > 0) {
            $embed='<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.facebook.com/plugins/video.php?href='.$url.'" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe></div>';
        } else {
            $embed= '<p>This URL <b class="color-red">'.$url.'</b> is not Facebook or Youtube link so please check it again and use the right one.</p>';
        }
        return $embed;
    }

    function search(Request $request) {
        if($request->ajax()) {
            $key_search = trim($request->key_search);
            $data_search = Article::where('title', 'LIKE', "%{$key_search}%")
                ->where('status','PUBLISHED')
                ->orderBy('date','DESC')
                ->get();
            // view ajax
            return view('ajax_search', compact('data_search','key_search'));
        }
    }

}
