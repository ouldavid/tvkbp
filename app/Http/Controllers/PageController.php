<?php

namespace App\Http\Controllers;

use App\Models\Program;
use App\Models\Video;
use Backpack\NewsCRUD\app\Models\Article;
use Backpack\PageManager\app\Models\Page;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index($slug, $subs = null)
    {
        $page = Page::findBySlug($slug == null ? '/':$slug);
        $articles=null;$programs=null;$videos=null;

        if (!$page)
        {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $this->data['title'] = $page->title;
        $this->data['page'] = $page->withFakes();

        if($page->template == 'home'){
            $articles = Article::where('status','PUBLISHED')
                ->orderBy('created_at','DESC')
                ->get();
            $programs = Program::where('status','PUBLISHED')
                ->orderBy('created_at','DESC')
                ->get();
            $videos = Video::where('status','PUBLISHED')
                ->orderBy('created_at','DESC')
                ->get();
        }

        return view($page->template, $this->data,compact(['articles','programs','videos']));
    }
}