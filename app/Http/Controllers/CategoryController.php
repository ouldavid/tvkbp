<?php

namespace App\Http\Controllers;

use Backpack\NewsCRUD\app\Models\Article;
use Backpack\NewsCRUD\app\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @param $slug
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($slug)
    {
        $category = Category::findBySlug($slug);

        if (!$category)
        {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $articles = Article::where(['category_id'=>$category->id,'status'=>'PUBLISHED'])
            ->orderBy('created_at','DESC')
            ->get();
        return view('category', compact(['category','articles']));
    }

    public function show(Request $request)
    {
        $article = Article::find($request->id);
        return view('article',compact('article'));
    }
}
